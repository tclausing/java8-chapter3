package slide30;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Uses interfaces to ensure all required parameters are provided, similar to a
 * constructor with arguments
 */
public class StrictConnectionBuilder {

	interface NeedsUser {
		NeedsPassword user(String user);
	}
	
	interface NeedsPassword {
		NeedsDBMS password(String password);
	}
	
	interface NeedsDBMS {
		NeedsServer dbms(String dbms);
	}
	
	interface NeedsServer {
		NeedsPort server(String server);
	}
	
	interface NeedsPort {
		Connection port(int port);
	}
	
	public static NeedsUser instance() {
		return new Builder();
	}
	
	static class Builder implements NeedsUser, NeedsPassword, NeedsDBMS, NeedsServer, NeedsPort {
	
		private Properties properties = new Properties();
		private String dbms;
		private String server;
		
		public Builder user(String user) {
			properties.setProperty("user",  user);
			return this;
		}
		
		public Builder password(String user) {
			properties.setProperty("password",  user);
			return this;
		}
		
		public Builder dbms(String dbms) {
			this.dbms = dbms;
			return this;
		}
		
		public Builder server(String server) {
			this.server = server;
			return this;
		}
		
		public Connection port(int port) {
			try {
				return DriverManager.getConnection("jdbc:" + dbms + "://" + server + ":" + port + "/", properties);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	public static void main(String[] args) throws SQLException {
		
		Connection connection = StrictConnectionBuilder.instance()
				.user("jim")
				.password("twinkletoes")
				.dbms("mongo")
				.server("myserver")
				.port(27017);
	}

}
