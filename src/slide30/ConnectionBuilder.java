package slide30;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionBuilder {
	
	private Properties properties = new Properties();
	private String dbms;
	private String server;
	private int port;
	
	public ConnectionBuilder user(String user) {
		properties.setProperty("user",  user);
		return this;
	}
	
	public ConnectionBuilder password(String user) {
		properties.setProperty("password",  user);
		return this;
	}
	
	public ConnectionBuilder dbms(String dbms) {
		this.dbms = dbms;
		return this;
	}
	
	public ConnectionBuilder server(String server) {
		this.server = server;
		return this;
	}
	
	public ConnectionBuilder port(int port) {
		this.port = port;
		return this;
	}
	
	public Connection connection() throws SQLException {
		return DriverManager.getConnection("jdbc:" + dbms + "://" + server + ":" + port + "/", properties);
	}

	
	public static void main(String[] args) throws SQLException {
		
		Connection connection = new ConnectionBuilder()
				.user("jim")
				.password("twinkletoes")
				.dbms("mongo")
				.server("myserver")
				.dbms("jkflds") // oops
				.port(27017)
				.connection();
	}

}
