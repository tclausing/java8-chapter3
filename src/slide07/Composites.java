package slide07;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.IntPredicate;

public class Composites {

	public static void main(String[] args) {

		System.out.println("===== Function composition =====");
		
		Function<Integer, Integer> baseFunction = t -> t + 2;

		Function<Integer, Integer> afterFunction = baseFunction.andThen(t -> t * 3);
		System.out.println(afterFunction.apply(5));

		Function<Integer, Integer> beforeFunction = baseFunction.compose(t -> t * 3);
		System.out.println(beforeFunction.apply(5));
		
		
		
		System.out.println("\n===== Predicate composition =====");
		IntPredicate evenGreaterThanFive = 
				((IntPredicate) n -> (n % 2) == 0)
						.and(n -> n > 5);
		
		System.out.println(
			Arrays.stream(new int[] {1, 2, 3, 4, 5, 6, 7, 8})
					.anyMatch(evenGreaterThanFive));
	}
}
