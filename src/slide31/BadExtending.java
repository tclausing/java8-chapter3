package slide31;

public class BadExtending {
	
	static class Boat {
		private String name;
		
		public Boat name(String name) {
			this.name = name;
			return this;
		}
		
		public String name() {
			return name;
		}
	}
	
	static class SailBoat extends Boat {
		private int numberOfSails;
		
		public SailBoat numberOfSails(int numberOfSails) {
			this.numberOfSails = numberOfSails;
			return this;
		}
		
		public int numberOfSails() {
			return numberOfSails;
		}
	}
	
	
	public static void main(String[] args) {

		new SailBoat()
				.name("HMS Compilation Error")
				.numberOfSails(4);
		
		new SailBoat()
				.numberOfSails(4)
				.name("HMS Compilation Error");
	}

}
