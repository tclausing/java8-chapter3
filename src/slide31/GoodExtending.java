package slide31;

public class GoodExtending {
	
	static class Boat<DERIVED extends Boat<DERIVED>> {
		private String name;
		
		@SuppressWarnings("unchecked")
		public DERIVED name(String name) {
			this.name = name;
			return (DERIVED) this;
		}
		
		public String name() {
			return name;
		}
	}
	
	static class SailBoat extends Boat<SailBoat> {
		private int numberOfSails;
		
		public SailBoat numberOfSails(int numberOfSails) {
			this.numberOfSails = numberOfSails;
			return this;
		}
		
		public int numberOfSails() {
			return numberOfSails;
		}
	}
	
	
	public static void main(String[] args) {
		
		new SailBoat()
				.name("HMS Dauntless")
				.numberOfSails(4);
	}

}
