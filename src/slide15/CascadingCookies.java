package slide15;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CascadingCookies {
	
	public static class Cookies {
		private List<String> ingredients = new ArrayList<>();
		
		public static Cookies getInstance() {
			return new Cookies();
		}
		
		public Cookies addIngredient(String ingredient) {
			ingredients.add(ingredient);
			return this;
		}
		
		public void bake() {
			System.out.println("Cascading Cookies with "
					+ ingredients.stream().collect(Collectors.joining(", "))
					+ ", SCRUMPTIOUS!!!");
		}
	}

	public static void main(String[] args) {
		Cookies.getInstance()
				.addIngredient("Eggs")
				.addIngredient("Flour")
				.addIngredient("Sugar")
				.addIngredient("Chocolate Chips")
				.bake();
		
		System.out.println("\n======= With initializer block =======");
		new Cookies() {
			{
				addIngredient("initializer eggs");
				bake();
			}
		};
	}

}
