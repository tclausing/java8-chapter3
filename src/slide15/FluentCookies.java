package slide15;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FluentCookies {
	
	public static class Cookies {
		private List<String> ingredients = new ArrayList<>();
		
		public static Cookies startMakinSome() {
			return new Cookies();
		}
		
		public Cookies withIngredient(String ingredient) {
			ingredients.add(ingredient);
			return this;
		}
		
		public void andBake() {
			System.out.println("Fluent Cookies with "
					+ ingredients.stream().collect(Collectors.joining(", "))
					+ ", SCRUMPTIOUS!!!");
		}
	}

	public static void main(String[] args) {
		Cookies.startMakinSome()
				.withIngredient("Eggs")
				.withIngredient("Flour")
				.withIngredient("Sugar")
				.withIngredient("Chocolate Chips")
				.andBake();
		
		System.out.println("\n======= With initializer block =======");
		new Cookies() {
			{
				withIngredient("initializer eggs");
				andBake();
			}
		};
	}

}
