package slide15;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Chaining {

	public static void main(String[] args) {
		
		
		System.out.println("====== stream chaining ======");
		int totalGreaterThan6 = Arrays.stream(new int[] {8, 12, 8, 6, 6, 5, 6, 0})
				.filter(n -> n > 6)
				.sum();
		System.out.println(totalGreaterThan6);

		
		System.out.println("\n====== Different instances returned each time = chaining ======");
		IntStream stream = Arrays.stream(new int[] {8, 12, 8, 6, 6, 5, 6, 0});
		System.out.println(stream);
		stream = stream.filter(n -> n > 6);
		System.out.println(stream);
		
		
		System.out.println("\n====== LocalDateTime chaining ======");
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime then = now
				.plusDays(3)
				.minusHours(4)
				.plusWeeks(1)
				.plusYears(2);
		System.out.println(now + "  -> \n" + then);

		
		System.out.println("\n====== String chaining ======");
		System.out.println(
				"Cat"
				.concat("Dog")
				.toLowerCase());
	}

}
