package slide09;

public class Salesman {

	private Manager manager;
	private String email;

	public Salesman(Manager manager) {
		this.manager = manager;
	}

	public Salesman(Manager manager, String email) {
		this.manager = manager;
		this.email = email;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
