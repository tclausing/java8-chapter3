package slide09;

import java.util.function.Function;

public class Main {

	public static void main(String[] args) {

		Function<Salesman, Manager> salesmanToManager = Salesman::getManager;
		Function<Manager, String> managerToEmail = Manager::getEmailAddress;
		Function<Salesman, String> salesmanToManagerEmail = salesmanToManager.andThen(managerToEmail);
		
		Manager manager = new Manager("manager@acme.com");
		Salesman salesman = new Salesman(manager, "salesman@acme.com");
		
		System.out.println(salesmanToManagerEmail.apply(salesman));
		System.out.println(salesman.getManager().getEmailAddress());
		
		
		System.out.println("\n======== with function passing ========");
		printOutputOf(salesman, salesmanToManagerEmail);
		printOutputOf(salesman, Salesman::getEmail);
	}
	
	public static void printOutputOf(Salesman salesman, Function<Salesman, String> salesmanToString) {
		System.out.println(salesmanToString.apply(salesman));
	}
}
